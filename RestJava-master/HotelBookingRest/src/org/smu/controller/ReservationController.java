package org.smu.controller;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.smu.dao.ReservationDAO;
import org.smu.dao.JDBCReservationDAO;
import org.smu.model.Reservation;
import org.smu.model.Error;

@Path("/Reservation")
public class ReservationController {

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getReservation() {
    	System.out.println("reservationcreate get");
    	
    	ReservationDAO dao = new JDBCReservationDAO();
        List<Reservation> listOfReservation = dao.getAllReservations();
        if(listOfReservation.size() ==0) {
        	Error error = new Error();
    		error.setErrorMsg("No records found in the database");
        	return Response.status(Response.Status.NO_CONTENT).entity(error).build();
        }
        System.out.println(listOfReservation.toString());
        return Response.status(Response.Status.OK).entity(listOfReservation).build();
    }
    //Reservation rese
    @POST
    @Path("/create")
    @Produces({ MediaType.APPLICATION_JSON})
    public Response addReservation(Reservation rese) {
    	System.out.println("reservationcreate");
    	ReservationDAO dao = new JDBCReservationDAO();
    	System.out.println("reservationcreate");
    //	System.out.println(dao.toString());
    		try {
    			
		  dao.addReservation(rese);
		} catch (SQLException e) {
			System.out.println("ERROR while inserting Reservation"+e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
        return Response.status(Response.Status.NO_CONTENT).build();
    }
  }
