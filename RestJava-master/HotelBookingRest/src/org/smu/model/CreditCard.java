package org.smu.model;

public class CreditCard {

	private int idCard;
	private int id;
	private String creditType;
	private String cCName;
	private String cCNum;
	public String getcCNum() {
		return cCNum;
	}
	public void setcCNum(String cCNum) {
		this.cCNum = cCNum;
	}
	private String expireDate;

	
	public int getIdCard() {
		return idCard;
	}
	public void setIdCard(int idCard) {
		this.idCard = idCard;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getCreditType() {
		return creditType;
	}
	public void setCreditType(String creditType) {
		this.creditType = creditType;
	}
	public String getCCName() {
		return cCName;
	}
	public void setCCName(String cCName) {
		this.cCName = cCName;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	
	
}