package org.smu.model;

public class Reservation {

	private int id;
	private String checkInDate;
	private String checkOutDate;
	private int guessNum;
	private int numRooms;
	private int reservationId;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		id = id;
	}

	public String getCheckInDate() {
		return checkInDate;
	}
	public void setCheckInDate(String checkInDate) {
		this.checkInDate = checkInDate;
	}
	public String getCheckOutDate() {
		return checkOutDate;
	}
	public void setCheckOutDate(String checkOutDate) {
		this.checkOutDate = checkOutDate;
	}
	public int getGuessNum() {
		return guessNum;
	}
	public void setGuessNum(int guessNum) {
		this.guessNum = guessNum;
	}
	public int getNumRooms() {
		return numRooms;
	}
	public void setNumRooms(int numRooms) {
		this.numRooms = numRooms;
	}
	public int getReservationId() {
		return reservationId;
	}
	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}

	
}
