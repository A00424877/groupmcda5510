package org.smu.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.smu.connection.JDBC;
import org.smu.model.CreditCard;

public class JDBCCreditCardDAO implements CreditCardDAO{


	@Override
	public CreditCard addCC(CreditCard CC) throws SQLException {

		PreparedStatement preparedStatement = null;
		String insertCreditCardSQL = "INSERT INTO mcda551003.CreditCard" + 
				" (Id,CreditType,CCName,CCNum,ExpireDate) VALUES"
				+ "(?,?,?,?,?)";
		try {

			 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			 java.util.Date date = new java.util.Date();
			 try {
				date = formatter.parse(CC.getExpireDate());
			 } catch (ParseException e) {
				e.printStackTrace();
			 }
			 java.sql.Date cexpdate = new java.sql.Date(date.getTime());  
			 

			preparedStatement = JDBC.getConnection().prepareStatement(insertCreditCardSQL);
			preparedStatement.setInt(1,CC.getId());
			preparedStatement.setString(2,CC.getCreditType());
			preparedStatement.setString(3,CC.getCCName());
			preparedStatement.setString(4,CC.getcCNum());
			preparedStatement.setDate(5,cexpdate);
			System.out.println(preparedStatement.toString());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}
		return CC;
	}
/*
	@Override
	public Customer updateCustomer(Customer cust) throws SQLException {
		PreparedStatement preparedStatement = null;
		String updateCustomerSQL = "UPDATE test.Customer SET firstName =?, lastName=?, city=?,province=?,country=?,postal=?,phone=?,email=?,address=? WHERE id=?";
		try {
				preparedStatement = JDBC.getConnection().prepareStatement(updateCustomerSQL);
				
				preparedStatement.setString(1,cust.getFirstName());
				preparedStatement.setString(2,cust.getLastName());
				preparedStatement.setString(3,cust.getCity());
				preparedStatement.setString(4,cust.getProvince());
				preparedStatement.setString(5,cust.getCountry());
				preparedStatement.setString(6,cust.getPostal());
				preparedStatement.setString(7,cust.getPhone());
				preparedStatement.setString(8,cust.getEmail());
				preparedStatement.setString(9,cust.getStreetNumber());
				preparedStatement.setInt(10, cust.getId());
				
				preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}
		return cust;
	}

	@Override
	public void deleteCustomer(int id) throws SQLException {
		PreparedStatement preparedStatement = null;
		String updateCustomerSQL = "DELETE from test.Customer WHERE id=?";
		try {
				preparedStatement = JDBC.getConnection().prepareStatement(updateCustomerSQL);
				preparedStatement.setInt(1, id);
				preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}
	}
*/
}
