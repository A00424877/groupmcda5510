package org.smu.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;


import org.smu.connection.JDBC;
import org.smu.model.Reservation;

public class JDBCReservationDAO implements ReservationDAO{

	@Override
	public List<Reservation> getAllReservations() {
		List<Reservation> Reservations = new ArrayList<Reservation>();
		try {
			Statement statement = JDBC.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM mcda551003.Reservation");
			 System.out.println(resultSet.toString());
				
				
			Reservation Reservation = null;
			while(resultSet.next()){
				
				Reservation = new Reservation();
				Reservation.setId(resultSet.getInt("id"));
				Reservation.setCheckInDate(resultSet.getString("CheckInDate"));
				Reservation.setCheckOutDate(resultSet.getString("CheckInDate"));
				Reservation.setGuessNum(resultSet.getInt("GuessNum"));
				Reservation.setNumRooms(resultSet.getInt("NumRooms"));
				Reservation.setReservationId(resultSet.getInt("ReservationId"));
						
				Reservations.add(Reservation);
			}
			resultSet.close();
			statement.close();

		} catch (SQLException e) {
			System.out.println(e);
		}
		return Reservations;
	}

	@Override
	public Reservation addReservation(Reservation rese) throws SQLException {

		PreparedStatement preparedStatement = null;
		String insertReservationSQL = "INSERT INTO mcda551003.Reservation" + 
				" (CheckInDate,CheckOutDate,GuessNum,NumRooms,Id) VALUES"
				+ "(?,?,?,?,?)";
		 System.out.println(rese.toString());
			
		
		try {
			
			
			
			preparedStatement = JDBC.getConnection().prepareStatement(insertReservationSQL);

			 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			 java.util.Date date = new java.util.Date();
			 try {
				date = formatter.parse(rese.getCheckInDate());
			 } catch (ParseException e) {
				e.printStackTrace();
			 }
			 java.sql.Date cindate = new java.sql.Date(date.getTime());  
			 

			 try {
				date = formatter.parse(rese.getCheckOutDate());
			 } catch (ParseException e) {
				e.printStackTrace();
			 }
			 java.sql.Date coutdate = new java.sql.Date(date.getTime());  
			 
			 System.out.println(rese.toString());
			
			preparedStatement.setDate(1,cindate);
			preparedStatement.setDate(2,coutdate);
			preparedStatement.setInt(3,rese.getGuessNum());
			preparedStatement.setInt(4,rese.getNumRooms());
			preparedStatement.setInt(5,rese.getId());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}
		return rese;
	}

	@Override
	public Reservation updateReservation(Reservation rese) throws SQLException {
		PreparedStatement preparedStatement = null;
		String updateReservationSQL = "UPDATE test.Reservation SET checkIn=?, checkOut=?, guessNum=?,numRooms=? WHERE id=?";
		try {
				preparedStatement = JDBC.getConnection().prepareStatement(updateReservationSQL);
				
				preparedStatement.setString(1,rese.getCheckInDate());
				preparedStatement.setString(2,rese.getCheckOutDate());
				preparedStatement.setInt(3,rese.getGuessNum());
				preparedStatement.setInt(4,rese.getNumRooms());
				preparedStatement.setInt(5, rese.getId());
				
				preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}
		return rese;
	}

	@Override
	public void deleteReservation(int id) throws SQLException {
		PreparedStatement preparedStatement = null;
		String updateReservationSQL = "DELETE from test.Reservation WHERE id=?";
		try {
				preparedStatement = JDBC.getConnection().prepareStatement(updateReservationSQL);
				preparedStatement.setInt(1, id);
				preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}
	}
}
