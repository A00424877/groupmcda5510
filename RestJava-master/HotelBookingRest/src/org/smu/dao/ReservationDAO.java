package org.smu.dao;

import java.sql.SQLException;
import java.util.List;

import org.smu.model.Reservation;

public interface ReservationDAO {

	List<Reservation> getAllReservations();
	
	Reservation addReservation(Reservation rese) throws SQLException;
	
	Reservation updateReservation(Reservation rese) throws SQLException;
	
	void deleteReservation(int id) throws SQLException;
}
