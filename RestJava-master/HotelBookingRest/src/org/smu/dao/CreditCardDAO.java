package org.smu.dao;

import java.sql.SQLException;
import java.util.List;

import org.smu.model.CreditCard;

public interface CreditCardDAO {

	//List<Customer> getAllCustomers();
	
	CreditCard addCC(CreditCard CC) throws SQLException;
	
	//CreditCard updateCC(CreditCard CC) throws SQLException;
	
	//void deleteCC(int Id) throws SQLException;
}