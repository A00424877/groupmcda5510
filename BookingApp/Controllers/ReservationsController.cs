﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BookingApp;
using System.Net.Http;

namespace BookingApp.Models
{
    public class ReservationsController : Controller
    {
        //private BookingAppEntityModel db = new BookingAppEntityModel();

        private BookingAppEntityModel db = new BookingAppEntityModel();
        private string BASE_URL = "http://localhost:8080/HotelBookingRest/rest/Reservation/"; 
        private string URL_INS = "http://localhost:8080/HotelBookingRest/rest/Reservations/insert";


        private HttpClient client = new HttpClient();

        // GET: Reservations
        //public ActionResult Index()
        //{
        //    var reservations = db.Reservations.Include(r => r.Contact);
        //    return View(reservations.ToList());
        //}

        public ActionResult Index()
        {
            client.BaseAddress = new Uri(BASE_URL);

            HttpResponseMessage response = client.GetAsync(BASE_URL).Result;
            List<Reservation> reservations = new List<Reservation>();
            if (response.IsSuccessStatusCode)
            {
                reservations = response.Content.ReadAsAsync<List<Reservation>>().Result;

                return View(reservations.ToList());
            }

            return View(reservations.ToList());
        }


        // GET: Contacts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservat = db.Reservations.Find(id);
            if (reservat == null)
            {
                return HttpNotFound();
            }
            return View(reservat);
        }
        // GET: Reservations/Details/5
        // public ActionResult Details(int? id)
        /* {
             if (id == null)
             {
                 return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
             }
             Reservation reservation = db.Reservations.Find(id);
             if (reservation == null)
             {
                 return HttpNotFound();
             }
             return View(reservation);
         }*/

        // GET: Reservations/Create
        public ActionResult Create(int contactId)
        {
            ViewBag.Id = new SelectList(db.Contacts, "Id", "LastName");
            TempData["contactID"] = contactId;
            return View();
        }

        // POST: Reservations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        
            // Insert
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////       





        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ReservationId,Id,CheckInDate,CheckOutDate,GuessNum,NumRooms")] Reservation reservation)
        {
            client.BaseAddress = new Uri(URL_INS);
            Reservation newReservation = reservation;

            HttpRequestMessage request;///PostAsync(URL_INS).Result;

          
                int contactID = (int)TempData["contactID"];
                Contact contact = db.Contacts.Find(contactID);
                request = client.PostAsync(URL_INS);
                //newReservation.Contact.Id = Id;
                //response.Content.addReservation<Reservation>;
                // response.Content.ReadAsAsync<List<Reservation>>().Result;
                newReservation.Contact = contact;


                return RedirectToAction("Create", "CreditCards", new { contactId = contactID });
             
                /*

            if (ModelState.IsValid)
            {
                int contactID = (int)TempData["contactID"];
                Contact contact = db.Contacts.Find(contactID);
                Reservation newReservation = reservation;
                //newReservation.Contact.Id = contactId;
                newReservation.Contact = contact;
                db.Reservations.Add(newReservation);
                db.SaveChanges();
                return RedirectToAction("Create", "CreditCards", new {contactId = contactID});
            }

            ViewBag.Id = new SelectList(db.Contacts, "Id", "LastName", reservation.Id);
            */

            return View(reservation);
        }


        /// <summary>
        /// /////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>



        // GET: Reservations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservation = db.Reservations.Find(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id = new SelectList(db.Contacts, "Id", "LastName", reservation.Id);
            return View(reservation);
        }

        // POST: Reservations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ReservationId,Id,CheckInDate,CheckOutDate,GuessNum,NumRooms")] Reservation reservation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reservation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id = new SelectList(db.Contacts, "Id", "LastName", reservation.Id);
            return View(reservation);
        }

        // GET: Reservations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservation = db.Reservations.Find(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            return View(reservation);
        }

        // POST: Reservations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Reservation reservation = db.Reservations.Find(id);
            db.Reservations.Remove(reservation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
