<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using BookingApp;
using BookingApp.Models;

namespace BookingApp.Controllers
{
    public class ContactsController : Controller
    {
        private BookingAppEntityModel db = new BookingAppEntityModel();
        private string BASE_URL = "http://localhost:8080/HotelBookingRest/rest/Customers/"; // http://localhost:8080/HotelBookingRest/rest/Customers
        private HttpClient client = new HttpClient();

        // GET: Contacts
        public ActionResult Index()
        {
            client.BaseAddress = new Uri(BASE_URL);

            HttpResponseMessage response = client.GetAsync(BASE_URL).Result;
            List<Contact> contacts = new List<Contact>();
            if (response.IsSuccessStatusCode)
            {
                 contacts = response.Content.ReadAsAsync<List<Contact>>().Result;

                return View(contacts.ToList());
            }

            return View(contacts.ToList());
        }

        // GET: Contacts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        /// <summary>
        /// ////////////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <returns></returns>

        // GET: Contacts/Create
        public ActionResult Create()
        {
            Contact contact = new Contact();

            var countries = GetAllCountries();
            contact.CountriesList = GetSelectListItems(countries);

            return View(contact);
        }

        // POST: Contacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,LastName,FirstName,StreetNumber,City,ProvinceState,Country,PostalCode,PhoneNumber,Email")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                db.Contacts.Add(contact);
                db.SaveChanges();
                //return Redirect("/reservations/Create");
                return RedirectToAction("Create", "Reservations", new { contactId = contact.Id });
            }

            var countries = GetAllCountries();
            contact.CountriesList = GetSelectListItems(countries);

            return View(contact);
        }

        // GET: Contacts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: Contacts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,LastName,FirstName,StreetNumber,City,ProvinceState,Country,PostalCode,PhoneNumber,Email")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contact).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(contact);
        }

        // GET: Contacts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: Contacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contact contact = db.Contacts.Find(id);
            db.Contacts.Remove(contact);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private IEnumerable<string> GetAllCountries()
        {
            return new List<string>
            {
                "USA",
                "CANADA"
            };
        }

        private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<string> elements)
        {
            var selectList = new List<SelectListItem>();
            foreach (var element in elements)
            {
                selectList.Add(new SelectListItem
                {
                    Value = element,
                    Text = element
                });
            }

            return selectList;
        }
    }
}
=======
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BookingApp;
using BookingApp.Models;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace BookingApp.Controllers
{
    public class ContactsController : Controller
    {
        private BookingAppEntityModel db = new BookingAppEntityModel();


        // GET: Contacts
        public async Task<ActionResult> Index()
        {
            List<Contact> custInfo = new List<Contact>();
            using (var client = new HttpClient())
            {
                //Passing service base url 
                client.BaseAddress = new Uri("http://localhost:8080/HotelBookingRest/rest/");
                client.DefaultRequestHeaders.Clear();
                //Define request data format 
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //Sending request to find web api REST service resource GetAllEmployees using HttpClient 
                HttpResponseMessage Res = await client.GetAsync("Customers");
                //Checking the response is successful or not which is sent using HttpClient 
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api  
                    var CustResponse = Res.Content.ReadAsStringAsync().Result;
                    //Deserializing the response recieved from web api and storing into the Employee list 
                    custInfo = JsonConvert.DeserializeObject<List<Contact>>(CustResponse);

                    System.Diagnostics.Debug.WriteLine(CustResponse);
                }

                //returning the employee list to view 
                return View(custInfo);
            }
        }
        /*  public ActionResult Index()
    {
        return View(db.Contacts.ToList());
    }
    */
        // GET: Contacts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // GET: Contacts/Create
        public ActionResult Create()
        {
            Contact contact = new Contact();

            var countries = GetAllCountries();
            contact.CountriesList = GetSelectListItems(countries);

            return View(contact);
        }

        // POST: Contacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Contact contact)
        {
            System.Diagnostics.Debug.WriteLine("++++++++++++++++++++++++++++++++");

            System.Diagnostics.Debug.WriteLine(contact.ToString());

            try
            {
                using (var client = new HttpClient())
                {
                    string addcustomerMethod = "Customers/create";
                   
                    String Baseurl = "http://localhost:8080/HotelBookingRest/rest/";
                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync(Baseurl + addcustomerMethod, contact);
                    string strJson = "";
                    var customerID = "";
                    if (responseMessage.IsSuccessStatusCode)
                    {
                         using (HttpContent content = responseMessage.Content)
                         {
                             // ... Read the string.
                             strJson = content.ReadAsStringAsync().Result;
                             dynamic jObj = (JObject)JsonConvert.DeserializeObject(strJson);
 //                           System.Diagnostics.Debug.WriteLine(jObj.ToString());

                             customerID = jObj["id"];
                        }
                        //Login loginObj = new Login();
                        //loginObj.CustomerId = customer.ID;
                        // Session["custId"] = Convert.ToString(customerID);
                        /*loginObj.loginId = Request["loginName"].ToString();
                        loginObj.password = Request["password"].ToString();
                        db.Logins.Add(loginObj);
                        db.SaveChanges();*/

                        System.Diagnostics.Debug.WriteLine("++++++++++++++"+customerID);

                        return RedirectToAction("Create", "Reservations", new { contactId = customerID });
                    }
                    else
                    {
                        return View("~/Views/Shared/Error.cshtml");
                    }
                }
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }



        /*    public ActionResult Create([Bind(Include = "Id,LastName,FirstName,StreetNumber,City,ProvinceState,Country,PostalCode,PhoneNumber,Email")] Contact contact)
            {
                if (ModelState.IsValid)
                {
                    db.Contacts.Add(contact);
                    db.SaveChanges();
                    //return Redirect("/reservations/Create");
                    return RedirectToAction("Create", "Reservations", new { contactId = contact.Id });
                }

                var countries = GetAllCountries();
                contact.CountriesList = GetSelectListItems(countries);

                return View(contact);
            }
            */
        // GET: Contacts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: Contacts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,LastName,FirstName,StreetNumber,City,ProvinceState,Country,PostalCode,PhoneNumber,Email")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contact).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(contact);
        }

        // GET: Contacts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: Contacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contact contact = db.Contacts.Find(id);
            db.Contacts.Remove(contact);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private IEnumerable<string> GetAllCountries()
        {
            return new List<string>
            {
                "USA",
                "CANADA"
            };
        }

        private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<string> elements)
        {
            var selectList = new List<SelectListItem>();
            foreach (var element in elements)
            {
                selectList.Add(new SelectListItem
                {
                    Value = element,
                    Text = element
                });
            }

            return selectList;
        }
    }
}
>>>>>>> d318b9097397298542bc97ad89c156237689f560
