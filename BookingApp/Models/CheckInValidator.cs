﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookingApp.Models
{
    public class CheckInValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var pickedDate = (DateTime) value;//testing
            var currentDate = DateTime.Today;

            if (currentDate > pickedDate)
            {
                return new ValidationResult("Wrong date. The date passed");
            }

            return ValidationResult.Success;
        }
    }
}