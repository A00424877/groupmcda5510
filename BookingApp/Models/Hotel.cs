﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;


namespace BookingApp.Models
{
    [Table("Hotel") ]
    public class Hotel
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HotelId { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Hotel Name cannot be longer than 50 characters.")]
        public string HotelName { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Phone Number cannot be longer than 20 characters.")]
        [RegularExpression(@"^[0-9A''-'\s]{1,20}$",
         ErrorMessage = "Characters are not allowed.")]

        public string PhoneNumber { get; set; }

        [Required]
        [RegularExpression(@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$",
         ErrorMessage = "Invalid email")]
        
        public string Email { get; set; }


        public byte[] Image { get; set; }





    }
}