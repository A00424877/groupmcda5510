﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace BookingApp.Models
{
    public class PhoneValidator : ValidationAttribute
    {
        private string countryAttr;

        public PhoneValidator(string countryAttr) : base("Invalid Phone Number Format")
        {
            this.countryAttr = countryAttr;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            PropertyInfo propertyInfo = validationContext.ObjectType.GetProperty(countryAttr);
            var countrySelection = (String)propertyInfo.GetValue(validationContext.ObjectInstance);
            Regex CndUsaPhoneNumRgx = new Regex(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$");
            try
            {
                var phoneNumber = value.ToString();

                if (phoneNumber != null)
                {

                    if ((countrySelection == "CANADA" || countrySelection == "USA") && CndUsaPhoneNumRgx.IsMatch(phoneNumber))
                    {
                        return ValidationResult.Success;
                    }
                    else if (!CndUsaPhoneNumRgx.IsMatch(phoneNumber))
                    {
                        return new ValidationResult("Invalid Phone Number");
                    }
                }
            }
            catch (Exception ex) {
                return new ValidationResult("Phone is required");
            }

            return ValidationResult.Success;
        }
    }
}