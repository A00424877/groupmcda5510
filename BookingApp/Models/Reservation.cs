﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Web.UI;

namespace BookingApp.Models
{
    [Table("Reservation")]

    public class Reservation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ReservationId { get; set; }

       // [ForeignKey("Contact")]
        public int Id{ get; set; }
        public virtual  Contact Contact { get; set; }


        [Required]
        [DataType(DataType.Date)]
        [CheckInValidator]
        public DateTime CheckInDate { get; set; }


        [Required]
        [DataType(DataType.Date)]
        [CheckInCheckOutValidator(nameof(CheckInDate))]
        public DateTime CheckOutDate { get; set; }

        [Required]
        [Range (1,10)]
        public int GuessNum { get; set; }

        [Required]
        [Range (1,10)]
        public int NumRooms { get; set; }   
    }
}