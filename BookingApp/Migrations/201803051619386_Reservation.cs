namespace BookingApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Reservation : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Contacts", newName: "Contact");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Contact", newName: "Contacts");
        }
    }
}
