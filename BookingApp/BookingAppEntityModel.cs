namespace BookingApp
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using BookingApp.Models;

    public partial class BookingAppEntityModel : DbContext
    {
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Hotel> Hotels { get; set; }
        public DbSet<CreditCard> CreditCards { get; set; }

        public BookingAppEntityModel()
            : base("name=BookingAppEntityModel")
        {
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
